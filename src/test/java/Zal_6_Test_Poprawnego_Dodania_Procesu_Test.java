import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Zal_6_Test_Poprawnego_Dodania_Procesu_Test extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getAddProcess() {
        return new Object[][]{
                {"description text", "notes text"},
                {"0", "notes text"},
                {"text description", ""}
        };
    }

    @Test(dataProvider = "getAddProcess")
    public void addProcessTest(String processDescription, String processNotes) {
        String processName = "proces" + UUID.randomUUID().toString().substring(0, 5);

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .typeDescription(processDescription)
                .typeNotes(processNotes)
                .submitCreate()
                .assertAddProcess(processName, processDescription, processNotes)
        ;
    }
}
