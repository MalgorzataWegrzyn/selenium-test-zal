import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

import java.util.UUID;

public class Zal_3_Test_Niepoprawnej_Rejestracji_Użytkownika_Haslo_Nie_Spelnia_Wymagan_Test extends SeleniumBaseTest {

    @DataProvider
    public Object[][] wrongPassword() {
        return new Object[][]{
                {"testowy2&", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Testowy&", "Passwords must have at least one digit ('0'-'9')."},
                {"haSlo102", "Passwords must have at least one non alphanumeric character."}
        };
    }

    @Test(dataProvider = "wrongPassword")
    public void incorrectRegisterWithWrongPassword(String password, String expErrorMessage) {
        String rightEmail = "test" + UUID.randomUUID().toString().substring(0, 3) + "@gmail.com";
        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(password)
                .registerWithFailure()
                .assertErrorPasswordUniversal(expErrorMessage)
        ;
    }

    @Test
    public void incorrectRegisterWithWrongPasswordWithAllConditions() {
        String rightEmail = "test" + UUID.randomUUID().toString().substring(0, 3) + "@gmail.com";
        String password = "testowy";

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(password)
                .registerWithFailure()
                .assertErrorPasswordUniversal("Passwords must have at least one non alphanumeric character.")
                .assertErrorPasswordUniversal("Passwords must have at least one digit ('0'-'9').")
                .assertErrorPasswordUniversal("Passwords must have at least one uppercase ('A'-'Z').")
        ;
    }

    @Test
    public void incorrectRegisterWithWrongPasswordWithoutCapitalLetterAndDigit() {
        String rightEmail = "test" + UUID.randomUUID().toString().substring(0, 3) + "@gmail.com";
        String password = "testowe!";

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(password)
                .registerWithFailure()
                .assertErrorPasswordUniversal("Passwords must have at least one uppercase ('A'-'Z').")
                .assertErrorPasswordUniversal("Passwords must have at least one digit ('0'-'9').")
        ;
    }

    @Test
    public void incorrectRegisterWithWrongPasswordWithoutCapitalLetterAndSpecialMark() {
        String rightEmail = "test" + UUID.randomUUID().toString().substring(0, 3) + "@gmail.com";
        String password = "testowe2";

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(password)
                .registerWithFailure()
                .assertErrorPasswordUniversal("Passwords must have at least one uppercase ('A'-'Z').")
                .assertErrorPasswordUniversal("Passwords must have at least one non alphanumeric character.")
        ;
    }

    @Test
    public void incorrectRegisterWithWrongPasswordWithoutDigitAndSpecialMark() {
        String rightEmail = "test" + UUID.randomUUID().toString().substring(0, 3) + "@gmail.com";
        String password = "testoweZ";

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(password)
                .registerWithFailure()
                .assertErrorPasswordUniversal("Passwords must have at least one digit ('0'-'9').")
                .assertErrorPasswordUniversal("Passwords must have at least one non alphanumeric character.")
        ;
    }

    @Test
    public void incorrectRegisterWithWrongPasswordTooShort() {
        String rightEmail = "test" + UUID.randomUUID().toString().substring(0, 3) + "@gmail.com";
        String password = "teS1";

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(password)
                .registerWithFailure()
                .assertErrorPasswordUniversal("The Password must be at least 6 and at max 100 characters long.")
        ;
    }

    @Test
    public void incorrectRegisterWithWrongPasswordTooLong() {
        String rightEmail = "test" + UUID.randomUUID().toString().substring(0, 3) + "@gmail.com";
        String password = "teS1" + password100();

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(password)
                .registerWithFailure()
                .assertErrorPasswordUniversal("The Password must be at least 6 and at max 100 characters long.")
        ;
    }

    @Test
    public void incorrectRegisterWithWrongPasswordWithoutLowercase() {
        String rightEmail = "test" + UUID.randomUUID().toString().substring(0, 3) + "@gmail.com";
        String password = "ABS1#PR56";

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(password)
                .registerWithFailure()
                .assertErrorPasswordUniversal("Passwords must have at least one lowercase ('a'-'z').")
        ;
    }

    private String password100() {
        String password100 = UUID.randomUUID().toString().substring(0, 36) + UUID.randomUUID().toString().substring(0, 36) + UUID.randomUUID().toString().substring(0, 36);
        return password100;
    }


}
