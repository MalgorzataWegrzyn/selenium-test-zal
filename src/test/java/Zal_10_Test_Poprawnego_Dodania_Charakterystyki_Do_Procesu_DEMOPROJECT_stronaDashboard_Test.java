import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Zal_10_Test_Poprawnego_Dodania_Charakterystyki_Do_Procesu_DEMOPROJECT_stronaDashboard_Test extends SeleniumBaseTest {
    @Test
    public void addCharacteristicTestOnDashboard() {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 5);
        String lowerSL = "8";
        String upperSL = "10";

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristics()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLowerSL(lowerSL)
                .typeUpperSL(upperSL)
                .submitCreate()
                .goToDashboard()
                .assertShowCharacteristicToDashboard(characteristicName)
        ;
    }
}
