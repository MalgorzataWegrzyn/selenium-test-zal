import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Zal_8_Test_Niepoprawnego_Dodania_Procesu_Test extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getAddProcess() {
        return new Object[][]{
                {"" + UUID.randomUUID().toString().substring(0, 2), "The field Name must be a string with a minimum length of 3 and a maximum length of 30."},
                {"proces" + UUID.randomUUID().toString().substring(0, 25), "The field Name must be a string with a minimum length of 3 and a maximum length of 30."}
        };
    }

    @Test(dataProvider = "getAddProcess")
    public void addProcessTest(String processName, String expComment) {

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .submitCreateWithFailure()
                .assertAddProcessWithFailure(expComment)
                .backToList()
                .assertProcessIsNotShown(processName)
        ;
    }
}
