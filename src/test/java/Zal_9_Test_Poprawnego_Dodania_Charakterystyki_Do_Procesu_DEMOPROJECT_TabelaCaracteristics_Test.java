import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Zal_9_Test_Poprawnego_Dodania_Charakterystyki_Do_Procesu_DEMOPROJECT_TabelaCaracteristics_Test extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getCharacteristicsData(){
        return new Object[][]{
                {"8", "10", ""},
                {"0", "90", "2"},
                {"-1", "100", ""},
                {"-10", "-80", ""}
        };

    }
    @Test (dataProvider = "getCharacteristicsData")
    public void addProcessTest(String lowerSL, String upperSL, String binCount) {
        String processName = "DEMO PROJECT";
        String characteristicName = "Characteristic " + UUID.randomUUID().toString().substring(0,5);

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristics()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLowerSL(lowerSL)
                .typeUpperSL(upperSL)
                .typeBinCount(binCount)
                .submitCreate()
                .assertCharacteristic(processName, characteristicName, lowerSL, upperSL, binCount)
                ;
    }
}
