import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

import java.util.UUID;

public class Zal_2_Test_Niepoprawnej_Resjetracji_Uzytkownika_Test extends SeleniumBaseTest {

    @Test()
    public void incorrectRegister(){

        String rightEmail = "test" + UUID.randomUUID().toString().substring(0, 3) + "@gmail.com";
        String password = "Hasło1!";
        String confirmPassword = "Hasło1";

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(confirmPassword)
                .registerWithFailure()
                .assertErrorConfirmationPasswordIsShown()
        ;
    }
}
