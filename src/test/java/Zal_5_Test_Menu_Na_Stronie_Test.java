import org.testng.annotations.Test;
import pages.LoginPage;


public class Zal_5_Test_Menu_Na_Stronie_Test extends SeleniumBaseTest {

    @Test
    public void testMenu(){
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                    .assertProcessesUrl(config.getApplicationProcesses())
                    .assertProcessHeader()
                .goToCharacteristics()
                    .assertCharacteristicsUrl(config.getApplicationCharacteristics())
                    .assertCharacteristicHeader()
                .goToDashboard()
                    .assertDashboardUrl(config.getApplicationDashboard())
                    .assertDemoProjectIsShown()
        ;

    }

}
