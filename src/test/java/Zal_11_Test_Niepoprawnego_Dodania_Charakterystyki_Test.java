import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Zal_11_Test_Niepoprawnego_Dodania_Charakterystyki_Test extends SeleniumBaseTest {

    @Test
    public void addCharacteristicWithFailureTest() {
        String processName = "DEMO PROJECT";
        String characteristicName = "Characteristic " + UUID.randomUUID().toString().substring(0,5);
        String lowerSL = "8";


        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristics()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLowerSL(lowerSL)
                .submitCreateWithFailure()
                .assertAddCharacteristicError()
                .backToList()
                .assertAddCharacteristicIsNotShown(characteristicName)
        ;
    }
}
