import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

import java.util.UUID;

public class Zal_4_Test_Poprawnego_Logowania_Test extends SeleniumBaseTest {

    @Test
    public void correctLogin(){

        String rightEmail = "test" + UUID.randomUUID().toString().substring(0,3)+"@gmail.com";
        String password = "Hasło1!";
        String confirmPassword = "Hasło1!";

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(rightEmail)
                .typePassword(password)
                .typeConfirmPassword(confirmPassword)
                .registerWithSuccess()
                .assertWelcomeElementIsShown()
                .backToLoginPage()
                .assertSuccessfullyLoggedOutIsShown()
                .typeEmail(rightEmail)
                .typePassword(password)
                .submitLogin()
                .assertWelcomeElementIsShown()
        ;
    }
}
