import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class Zal_12_Test_Poprawnego_Dodania_Charakterystyki_and_Rezultatow_and_Sprawdzenie_Raportu_Test extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getAddCharacteristic() {
        return new Object[][]{
               {"1", "50", "Test4", "2.0;16.0;8.0;10.0"},
               {"5", "80", "Test5", "6.0;95.0;0.0;15.0;5.6"},
               {"20", "120", "Test6", "6.0;6.0;8.6;0.2;10.8;5.6"}
        };
    }
    private String calculateMean(String results){
       List<Double> doubleResult = Arrays.stream(results.split(";"))
                .map(Double::parseDouble)
                .collect(Collectors.toList());

       Double summ = doubleResult.stream()
                .reduce(0.0, (a, b) -> a + b);

       return String.valueOf(summ / doubleResult.size());
    }

    @Test (dataProvider = "getAddCharacteristic")
    public void addCharacteristicAndResultAndCheckReport(String lowerSL, String upperSL, String sampleName, String results) {
        String processName = "DEMO PROJECT";
        String characteristicName = "Characteristic" + UUID.randomUUID().toString().substring(0,3);

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristics()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLowerSL(lowerSL)
                .typeUpperSL(upperSL)
                .submitCreate()
                .goToResults(characteristicName)
                .clickAddResults()
                .typeSampleName(sampleName)
                .typeResults(results)
                .submitCreate()
                .backToCharacteristics()
                .goToReport(characteristicName)
                .assertMean(calculateMean(results))
        ;
    }
}
