import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Zal_7_Test_Poprawnego_Dodania_Procesu_Na_Dashboard_Test extends SeleniumBaseTest {

    @Test
    public void addProcessTest() {
        String processName = "proces" + UUID.randomUUID().toString().substring(0, 5);

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .submitCreate()
                .goToDashboard()
                .assertShowProcessToDashboard(processName)
        ;
    }
}
