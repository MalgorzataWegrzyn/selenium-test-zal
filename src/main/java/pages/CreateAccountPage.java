package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class CreateAccountPage {
    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(css = "#Password")
    private WebElement passwordTxt;

    @FindBy(css = "#ConfirmPassword" )
    private WebElement confirmPasswordTxt;

    @FindBy(css = "#Email-error")
    private WebElement emailError;

    @FindBy(css = "button[type = submit]")
    private WebElement registerBtn;

    @FindBy(css = "#ConfirmPassword-error")
    private WebElement ErrorConfirmPasswordTxt;

    @FindBy(css = "div.text-danger >ul>li")
    private WebElement passwordErrorTxt;

    @FindBy(css = "div.text-danger >ul>li")
    public List<WebElement> passwordErrors;

    public CreateAccountPage typeEmail(String email){
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage typePassword(String password){
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public CreateAccountPage typeConfirmPassword(String password){
        confirmPasswordTxt.clear();
        confirmPasswordTxt.sendKeys(password);
        return this;
    }

    public HomePage registerWithSuccess(){
        registerBtn.click();
        return new HomePage(driver);
    }

    public CreateAccountPage registerWithFailure(){
        registerBtn.click();
        return this;
    }

    public CreateAccountPage assertErrorConfirmationPasswordIsShown() {
        Assert.assertTrue(ErrorConfirmPasswordTxt.isDisplayed(), "The password and confirmation password do not match." );
        return this;
    }

    public CreateAccountPage assertErrorPasswordIsShown(String messageTxt){
        Assert.assertTrue(passwordErrorTxt.isDisplayed(),messageTxt);
        return this;
    }

    public CreateAccountPage assertErrorPasswordWithAllConditionsIsShown(){
        List<WebElement> validationErrors = driver.findElements(By.cssSelector("div.text-danger >ul>li"));
            Assert.assertEquals(validationErrors.get(2).getText(), "Passwords must have at least one uppercase ('A'-'Z').");
            Assert.assertEquals(validationErrors.get(1).getText(), "Passwords must have at least one digit ('0'-'9').");
            Assert.assertEquals(validationErrors.get(0).getText(),"Passwords must have at least one non alphanumeric character.");
        return this;
    }

    public CreateAccountPage assertErrorPasswordUniversal(String text) {
        boolean doesErrorExist = passwordErrors
                .stream()
                .anyMatch(validationError -> validationError.getText().equals(text));

        Assert.assertTrue(doesErrorExist);
        return this;
    }

    public CreateAccountPage assertErrorPasswordWithoutCapitalLetterAndDigit(){
        List<WebElement> validationErrors = driver.findElements(By.cssSelector("div.text-danger >ul>li"));
        Assert.assertEquals(validationErrors.get(1).getText(), "Passwords must have at least one uppercase ('A'-'Z').");
        Assert.assertEquals(validationErrors.get(0).getText(), "Passwords must have at least one digit ('0'-'9').");
        return this;
    }

    public CreateAccountPage assertErrorPasswordWithoutCapitalLetterAndSpecialMark(){
        List<WebElement> validationErrors = driver.findElements(By.cssSelector("div.text-danger >ul>li"));
        Assert.assertEquals(validationErrors.get(1).getText(), "Passwords must have at least one uppercase ('A'-'Z').");
        Assert.assertEquals(validationErrors.get(0).getText(),"Passwords must have at least one non alphanumeric character.");
        return this;
    }

    public CreateAccountPage assertErrorPasswordWithoutDigitAndSpecialMark(){
        List<WebElement> validationErrors = driver.findElements(By.cssSelector("div.text-danger >ul>li"));
        Assert.assertEquals(validationErrors.get(1).getText(), "Passwords must have at least one digit ('0'-'9').");
        Assert.assertEquals(validationErrors.get(0).getText(),"Passwords must have at least one non alphanumeric character.");
        return this;
    }


}
