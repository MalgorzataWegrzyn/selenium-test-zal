package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashboardPage extends HomePage {

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    private String GENERIC_PROCESS_ROW_XPATH_DASHBOARD = "//h2[text()='%s']/..";
    private String GENERIC_CHARACTERISTIC_ROW_XPATH_DASHBOARD = "//p[contains(text(), '%s')]/..";
    private String GENERIC_DASHBOARD_ROW_XPATH_DASHBOARD = "Create process";

    @FindBy(css = "div.x_title > h2")
    private WebElement projectDashboard;

    @FindBy(xpath = "//h2[text()='DEMO PROJECT']")
    private WebElement demoProjectHeader;


    public DashboardPage assertDashboardUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl, "Check Page Dashboard URL");
        return this;
    }

    public DashboardPage assertDemoProjectIsShown() {
        Assert.assertTrue(demoProjectHeader.isDisplayed());
        return this;
    }

    public DashboardPage assertShowProcessToDashboard(String expProcessName) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH_DASHBOARD, expProcessName);
        WebElement processRow = driver.findElement(By.xpath(processXpath));

        Assert.assertEquals(processRow.getText(), expProcessName);
        return this;
    }

    public DashboardPage assertShowCharacteristicToDashboard(String expCharName) {
        String characteristicXpath = String.format(GENERIC_CHARACTERISTIC_ROW_XPATH_DASHBOARD, expCharName);
        WebElement characteristicRow = driver.findElement(By.xpath(characteristicXpath));

        Assert.assertEquals(characteristicRow.getText().trim(), expCharName);
        return this;
    }

}
