package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class ProcessesPage extends HomePage {

    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(linkText = "Add new process")
    private WebElement addProcessBtn;


    private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";

    public CreateProcessPage clickAddProcess() {
        addProcessBtn.click();
        return new CreateProcessPage(driver);
    }

    public ProcessesPage assertProcessesUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl, "Check Page Processes URL");
        return this;
    }

    public ProcessesPage assertProcessHeader() {
        Assert.assertEquals(pageHeader.getText(), "Processes");
        return this;
    }

    public ProcessesPage assertAddProcess(String expNameProcess, String expDescription, String expNotes) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expNameProcess);
        WebElement processRow = driver.findElement(By.xpath(processXpath));

        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();

        Assert.assertEquals(actDescription, expDescription);
        Assert.assertEquals(actNotes, expNotes);
        return this;
    }

    public ProcessesPage assertProcessIsNotShown(String processName) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, processName);
        List<WebElement> process = driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(process.size(), 0);
        return this;
    }


}
