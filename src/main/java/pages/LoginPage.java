package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class LoginPage {
    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(css = "#Password")
    private WebElement passwordTxt;

    @FindBy(css = "button[type = submit]")
    private WebElement loginBtn;

    @FindBy(css = "a[href*=Register]")
    private WebElement registerLink;

    @FindBy(css = ".flash-message>strong")
    private WebElement checkLogOut; //User succesfully logged out

    public CreateAccountPage goToRegisterPage() {
        registerLink.click();
        return new CreateAccountPage(driver);
    }

    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }

    public LoginPage assertSuccessfullyLoggedOutIsShown() {
        Assert.assertTrue(checkLogOut.isDisplayed(), "User succesfully logged out");
        return this;
    }
}
