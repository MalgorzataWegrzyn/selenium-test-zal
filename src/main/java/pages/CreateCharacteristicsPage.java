package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateCharacteristicsPage {
    protected WebDriver driver;

    public CreateCharacteristicsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "ProjectId")
    private WebElement projectSlc;

    @FindBy(id = "Name")
    private WebElement nameTxt;

    @FindBy(id = "LowerSpecificationLimit")
    private WebElement lowerSLtxt;

    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upperSLtxt;

    @FindBy(id = "HistogramBinCount")
    private WebElement binCountTxt;

    @FindBy(css = "input[type=submit]")
    private WebElement createBtn;

    @FindBy(xpath = "//input[@id='UpperSpecificationLimit']/../span")
    private WebElement errorComment;

    @FindBy(linkText = "Back to List")
    private WebElement backToList;

    public CreateCharacteristicsPage selectProcess(String processName) {
        new Select(projectSlc).selectByVisibleText(processName);
        return this;
    }

    public CreateCharacteristicsPage typeName(String characteristicName) {
        nameTxt.clear();
        nameTxt.sendKeys(characteristicName);
        return this;
    }

    public CreateCharacteristicsPage typeLowerSL(String lowerSL) {
        lowerSLtxt.clear();
        lowerSLtxt.sendKeys(lowerSL);
        return this;
    }

    public CreateCharacteristicsPage typeUpperSL(String upperSL) {
        upperSLtxt.clear();
        upperSLtxt.sendKeys(upperSL);
        return this;
    }

    public CreateCharacteristicsPage typeBinCount(String binCount) {
        binCountTxt.clear();
        binCountTxt.sendKeys(binCount);
        return this;
    }

    public CharacteristicsPage submitCreate() {
        createBtn.click();
        return new CharacteristicsPage(driver);
    }

    public CreateCharacteristicsPage submitCreateWithFailure() {
        createBtn.click();
        return this;
    }

    public CharacteristicsPage backToList() {
        backToList.click();
        return new CharacteristicsPage(driver);
    }

    public CreateCharacteristicsPage assertAddCharacteristicError() {
        Assert.assertEquals(errorComment.getText(), "The value '' is invalid.");
        return this;
    }

}
