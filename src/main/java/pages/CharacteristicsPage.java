package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class CharacteristicsPage extends HomePage {

    public CharacteristicsPage (WebDriver driver){
        super(driver);
    }

    @FindBy (linkText = "Add new characteristic")
    private WebElement addCharacteristicBtn;

    @FindBy(css = ".menu-home")
    private WebElement homeNav;

    @FindBy(linkText = "Dashboard")
    private WebElement dashboardMenu;


    private String GENERIC_CHARACTERISTIC_ROW_XPATH = "//tbody//td[text()='%s']/..";
    private String GENERIC_CHARACTERISTIC_RESULTS_XPATH = "//td[text()='%s']/..//a[contains(@href, 'Results')]";
    private String GENERIC_CHARACTERISTIC_REPORT_XPATH = "//td[text()='%s']/..//a[contains(@href, 'Report')]";



    public CreateCharacteristicsPage clickAddCharacteristics(){
        addCharacteristicBtn.click();
        return new CreateCharacteristicsPage(driver);
    }


    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));

        return parent.getAttribute("class").contains("active");
    }

    public DashboardPage goToDashboard() {
        if (!isParentExpanded(homeNav))
            homeNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(dashboardMenu));

        dashboardMenu.click();

        return new DashboardPage(driver);
    }

    public ReportPage goToReport(String characteristicName){
        String reportBtnXpath = String.format(GENERIC_CHARACTERISTIC_REPORT_XPATH,characteristicName);
        driver.findElement(By.xpath(reportBtnXpath)).click();
        return new ReportPage(driver);
    }

    public ResultsPage goToResults(String characteristicName){
        String resultsBtnXpath = String.format(GENERIC_CHARACTERISTIC_RESULTS_XPATH, characteristicName);
        driver.findElement(By.xpath(resultsBtnXpath)).click();

        return new ResultsPage(driver);
    }

    public CharacteristicsPage assertCharacteristic(String expProcessName, String expName, String expLowerSL, String expUpperSL, String expBinCount){
        String characteristicXpath = String.format(GENERIC_CHARACTERISTIC_ROW_XPATH, expName);
        WebElement characteristicRow = driver.findElement(By.xpath(characteristicXpath));

        String actProcessName = characteristicRow.findElement(By.xpath("./td[1]")).getText();
        String actLowerSL = characteristicRow.findElement(By.xpath("./td[3]")).getText();
        String actUpperSL = characteristicRow.findElement(By.xpath("./td[4]")).getText();
        String actBinCount = characteristicRow.findElement(By.xpath("./td[5]")).getText();

        Assert.assertEquals(actProcessName, expProcessName);
        Assert.assertEquals(actLowerSL, expLowerSL);
        Assert.assertEquals(actUpperSL, expUpperSL);
        Assert.assertEquals(actBinCount, expBinCount);

        return this;
    }
    public CharacteristicsPage assertCharacteristicsUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl, "Check Page Characteristics URL");
        return this;
    }

    public CharacteristicsPage assertCharacteristicHeader() {
        Assert.assertEquals(pageHeader.getText(), "Characteristics");
        return this;
    }



    public CharacteristicsPage assertAddCharacteristicIsNotShown (String expCharacteristicName){
        String characteristicXpath = String.format(GENERIC_CHARACTERISTIC_ROW_XPATH, expCharacteristicName);
        List<WebElement> characteristicRow = driver.findElements(By.xpath(characteristicXpath));
        Assert.assertEquals(characteristicRow.size(), 0);
        return this;
    }


}
